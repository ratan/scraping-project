from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from dirbot.items import Website
from scrapy.http import Request

DOMAIN = 'www.mouthshut.com'
URL = 'http://%s' % DOMAIN

class DmozSpider(BaseSpider):
    name = DOMAIN
    allowed_domains = [DOMAIN]
    start_urls = [
        URL
    ]

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        sites = hxs.select('//*')
        for url in hxs.select('//a/@href').extract():
            if not url.startswith('http://'):
                url= URL + url 
            if "http://www.mouthshut.com/review/" in str(url):
                for site in sites:
                    title = site.select(".//*[@id='middle']/div[3]/ul/li/h1/text()").extract()
                    if title :
                        print "Title :"+str(title)
                        summary = site.select("//span[@class='summary']/text()").extract()
                        print "summary :"+str(summary)
                        desc = site.select("//span[@class='description']/*/text()").extract()
                        print "desc :"+str(desc)
                        by = site.select("//span[@class='reviewer']/text()").extract()
                        print "By "+str(by)
                print "URL :"+str(url)
            yield Request(url, callback=self.parse)